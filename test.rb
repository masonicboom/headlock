#!/usr/bin/env ruby
require 'open3'

# Unit test threat models.

## Test not broken by single site operator having access to multiple plaintexts.
def site_operator_with_multiple_account_plaintexts(hasher)
  testname = __method__.to_s

  dirname = File.join(File.dirname(__FILE__), 'results', File.basename(hasher))
  Dir.mkdir(dirname) unless Dir.exists?(dirname)

  Open3.popen3(hasher) do |stdin, stdout, stderr, wait_thread|
    # Test cases.
    inputs = [
      ["secret", "site", "bob", "8"],
      ["secret", "site", "joe", "8"]
    ]

    # Track success status.
    success = true
    outputs = []

    # Produce test output.
    output_filename = "#{File.join(File.dirname(__FILE__), 'results', File.basename(hasher), testname)}.tsv"
    File.open(output_filename, "w") do |f|
      # Header.
      f.write ['secret', 'site ID', 'account ID', 'length', 'hash'].join("\t")
      f.write "\n"

      inputs.each do |cols|
        stdin.puts cols.join("\t")
        stdin.flush
        output = stdout.gets

        outputs << output

        f.write (cols + [output]).join("\t")

        # Blank output indicates failure.
        success = false if output.strip.empty?
      end
    end

    # Test success.
    success = false if outputs.all? { |o| o == outputs.first }

    # Report success status.
    success
  end
end


## Test multiple accounts on same site have different password.

## Test if can generate 4 digit numerics (PIN codes).

## Test if can generate 8 char alphanumerics.

## Test if can generate >8 char alpha, numeric, symbols.


tests = [
  :site_operator_with_multiple_account_plaintexts
]

File.open('results.md', 'w') do |f|
  f.write "# Test Results\n\n"

  # Table header.
  f.write "#{(['Name'] + tests).map {|t| t.to_s}.join("\t| ")}\n"
  f.write "-|-\n"

  # Results row.
  Dir[File.join(File.dirname(__FILE__), 'hashers', '*')].each do |hasher|
    hasher_name = File.basename(hasher)

    statuses = tests.map do |test|
      test_name = test.to_s
      success = send(test, hasher)
      success ? "Y" : "N"
    end

    cols = ["[#{hasher_name}](./hashers/#{hasher_name})"]
    cols += statuses.each_with_index.map { |s,i| "[#{s}](./results/#{hasher_name}/#{tests[i].to_s}.tsv)" }
    f.write "#{cols.join("\t| ")}\n"
  end

end
