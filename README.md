# Password Hashing

Files in the *hashers* directory must take as input on STDIN tab-separated values with one line per hash to produce, having columns "secret", "site ID", "account ID", "length" and produce a corresponding line of output on STDOUT for each input line, giving the resultant hash. An empty line of output indicates failure to produce a hash from the given inputs.

## Structure

  /hashers/HASHNAME - Program that operates on STDIN and STDOUT as described above.
  /results.md - Summary of results of testing hash algorithms in /hashes.
  /results/HASHNAME/TESTNAME.tsv - Tab-separated value file of test input and output for TESTNAME run against HASHNAME.

## Results

[Here.](./headlock/src/master/results.md)
